#![feature(exit_status_error)]
extern crate serde_json;
use std::{process::{Command, Child, Stdio}, io::ErrorKind, collections::HashMap, env, path::Path, time::{Duration, Instant}};
use chrono::Datelike;
use json::JsonValue;
use regex::Regex;
use thirtyfour::{*, prelude::{WebDriver, ElementQueryable}, common::capabilities::firefox::FirefoxPreferences};

fn get_string_value_with_interpolation(json: &JsonValue, injectables: &std::collections::HashMap<String, String>) -> Option<String>
{
	json.as_str().and_then(|json|
	{
		Some(interpolate_string(json, injectables))
	})
}

fn interpolate_string(val: &str, injectables: &std::collections::HashMap<String, String>) -> String
{	
	let mut ret_val = val.to_string();
	for (variable_name, variable_value) in env::vars().into_iter().chain(injectables.iter().map(|(a, b)| (a.to_owned(), b.to_owned())))
	{
		Regex::new(format!("{}{}{}", r"\{", variable_name, r"(\\(?P<escape_chars>[^}]+))?\}").as_str()).and_then(|regex|
		{
			ret_val = regex.replace_all(ret_val.as_str(), |captures: &regex::Captures|
			{
				captures.name("escape_chars")
					.map(|escaped| escaped.as_str())
					.and_then(|escaped| Some(escape_chars(variable_value.as_str(), '\\', escaped.chars())))
					.unwrap_or(variable_value.to_owned())
			}).to_string();
			Ok(())
		}).ok();
	}
	Regex::new(format!("{}[0-9]+{}", r"\{", r"\}").as_str()).and_then(|regex|
	{
		ret_val = regex.replace_all(ret_val.as_str(), |_: &regex::Captures| "").to_string();
		Ok(())
	}).ok();
	ret_val
}

fn escape_chars(raw: &str, escape_char: char, to_escape_chars: impl Iterator<Item = char> + Clone) -> String
{
	raw.chars().fold(Vec::<String>::new(), |acc, next|
	{
		let mut ret_val = acc.clone();
		if to_escape_chars.clone().any(|to_escape_char| to_escape_char == next)
		{
			ret_val.push(escape_char.to_string());
		}
		ret_val.push(next.to_string());
		return ret_val;
	}).join("")
}

static mut VERBOSE: bool = false;

macro_rules! log
{
	($important:expr, $($tts:tt)*) =>
	{
		unsafe
		{
			if $important || VERBOSE
			{
				println!($($tts)*);
			}
		}
	}
}

type BoxErr = Box<dyn std::error::Error>;

#[tokio::main]
async fn main()
{
	let mut args: Vec<String> = std::env::args().collect();
	let mut shift_arg = false;
	{
		let first_arg_opt = args.get(1);
		if first_arg_opt.is_some()
		{
			let first_arg = first_arg_opt.unwrap().as_str();
			if first_arg == "-v"
			{
				shift_arg = true;
				unsafe { VERBOSE = true; }
			}
		}
	}
	if shift_arg
	{
		args.remove(0);
	}
	if args.len() <= 1
	{
		log!(true, "Expects at least one argument for the task name.");
		return;
	}
	let task = args.remove(1);
	args.remove(0);
	let mut all_results = Vec::<Result<(), BoxErr>>::new();
	let server = launch_server();
	let result = match server
	{
		Ok(mut server) =>
		{
			let tmp_dir = env::var("TMPDIR").unwrap_or(format!("{}/neon-browser-puppet-tmp", env::current_dir().expect("Program had no CWD").to_str().unwrap()).to_string());
			std::fs::create_dir_all(&tmp_dir).expect("Could not create temp directory.");
			let profile_dir = env::var("PROFILE_DIR").unwrap_or(format!("{}/firefox-profile", tmp_dir).to_string());
			log!(false, "Using firefox profile at: {}", profile_dir);
			std::fs::create_dir_all(&profile_dir).expect("Could not create profile directory.");
			let download_dir = format!("{}/downloads", tmp_dir);
			std::fs::create_dir_all(&download_dir).expect("Could not create downloads directory.");
			let driver = create_driver(profile_dir.as_str(), download_dir.as_str()).await;
			let result = match driver
			{
				Ok(mut driver) =>
				{
					all_results.push(run_main(&mut driver, task.as_str(), &args, download_dir).await);
					all_results.push(destroy_driver(driver).await);
					all_results.push(server.kill().map_err(|err| -> BoxErr { Box::new(err) }));
					Ok(())
				},
				Err(err) => Err(err),
			};
			all_results.insert(0, result);
			Ok(())
		},
		Err(err) => Err(err),
	};
	all_results.insert(0, result);
	let errors: Vec<&mut BoxErr> = all_results
		.iter_mut()
		.filter(|result| result.is_err())
		.map(|result| result.as_mut().unwrap_err())
		.collect();
	errors.iter().for_each(|err| log!(true, "ERROR: {}", err));

	Command::new("killall").arg("geckodriver").output().ok();

	match errors.first()
	{
		None => Ok(()),
		Some(_) => Err(()),
	}.expect("Encountered error during run.");
}

async fn create_driver(profile_dir: &str, default_download_dir: &str) -> Result<WebDriver, BoxErr>
{
	let mut caps = DesiredCapabilities::firefox();
	caps.add_firefox_arg("--enable-automation")?;
	caps.add_firefox_arg("--profile")?;
	caps.add_firefox_arg(&profile_dir)?;
	let mut prefs = FirefoxPreferences::new();
	prefs.set("browser.download.dir", default_download_dir)?;
	prefs.set("browser.download.folderList", 2)?;
	prefs.set("pdfjs.disabled", true)?;
	prefs.set("plugin.disable_full_page_plugin_for_types", "application/pdf,application/vnd.adobe.xfdf,application/vnd.fdf,application/vnd.adobe.xdp+xml")?;
	caps.set_preferences(prefs)?;
	Ok(WebDriver::new("http://localhost:4444", caps).await?)
}

async fn destroy_driver(driver: WebDriver) -> Result<(), BoxErr>
{
	Ok(driver.quit().await?)
}

async fn run_main(driver: &mut WebDriver, task: &str, args: &Vec<String>, download_dir: String) -> Result<(), BoxErr>
{
	let dat = std::fs::read_to_string(Path::new(task).canonicalize()?)?;
	let task = json::parse(&dat)?;
	let mut injectables = HashMap::<String, String>::new();
	for (i, arg) in args.iter().enumerate()
	{
		injectables.insert(format!("{}", i), arg.to_string());
	}
	injectables.insert("downloadDir".to_string(), download_dir);
	run_task(driver, &task, &mut injectables, false, false).await
}

async fn new_tab(driver: &mut WebDriver) -> Result<WindowHandle, BoxErr>
{
	let ret_val = driver.window().await?;
	let tab = driver.new_tab().await?;
	driver.switch_to_window(tab).await?;
	Ok(ret_val)
}

async fn switch_to_last_tab(driver: &mut WebDriver) -> Result<Option<WindowHandle>, BoxErr>
{
	let ret_val = driver.window().await?;
	let tabs = driver.windows().await?;
	let tab = tabs.last().ok_or("Could not get list of tabs.")?.to_owned();
	driver.switch_to_window(tab).await?;
	let new_tab = driver.window().await?;
	if ret_val == new_tab {Ok(None)}
	else {Ok(Some(ret_val))}
}

async fn close_tab(driver: &mut WebDriver, handle: WindowHandle) -> Result<(), BoxErr>
{
	if driver.window().await.is_ok()
	{
		driver.close_window().await?;
	}
	driver.switch_to_window(handle).await?;
	Ok(())
}

#[async_recursion::async_recursion]
async fn run_task(driver: &mut WebDriver, task: &JsonValue, injectables: &mut std::collections::HashMap<String, String>, scoped: bool, tabbed: bool) -> Result<(), BoxErr>
{
	let handle = if tabbed { Some(new_tab(driver).await?) } else { None };
	if scoped
	{
		let mut clone = injectables.clone();
		run_task_inner(driver, task, &mut clone).await?;
	}
	else
	{
		run_task_inner(driver, task, injectables).await?;
	};
	if handle.is_some() { close_tab(driver, handle.unwrap()).await?; }
	Ok(())
}

#[async_recursion::async_recursion]
async fn run_task_inner(driver: &mut WebDriver, task: &JsonValue, injectables: &mut std::collections::HashMap<String, String>) -> Result<(), BoxErr>
{
	if !task.is_object() { Err(BoxErr::from("Task was not an object."))?; }
	let steps: Vec::<&JsonValue> = task["steps"].members().collect();
	let mut i = 0;
	while i < steps.len()
	{
		let step = *steps.get(i).unwrap();
		match driver.get_alert_text().await
		{
			Ok(text) =>
			{
				injectables.insert("alertText".to_string(), text);
			},
			Err(_) =>
			{
				driver.current_url().await.and_then(|url| Ok(injectables.insert("url".to_string(), url.to_string()))).ok();
				driver.title().await.and_then(|url| Ok(injectables.insert("title".to_string(), url.to_string()))).ok();
			}
		}
		let step_type = step["type"].as_str();
		let tabbed = step["tabbed"].as_bool();
		let scoped = step["scoped"].as_bool();
		let not = step["not"].as_bool().unwrap_or(false);
		let timeout = Duration::from_secs(step["timeout"].as_u64().unwrap_or(5));
		let wait_for = step["waitfor"].as_str().unwrap_or("timeout");
		let file = get_string_value_with_interpolation(&step["file"], &injectables);
		let selector = get_string_value_with_interpolation(&step["selector"], &injectables);
		let key = get_string_value_with_interpolation(&step["key"], &injectables);
		let match_value = get_string_value_with_interpolation(&step["match"], &injectables);
		let exec = get_string_value_with_interpolation(&step["exec"], &injectables);
		let cwd = get_string_value_with_interpolation(&step["cwd"], &injectables);
		let code = get_string_value_with_interpolation(&step["code"], &injectables);
		let exit_code = step["exit_code"].as_u8();
		let step_verbose = step["verbose"].as_bool().unwrap_or(false);
		let args: Vec<Option<String>> = step["args"].members().map(|val| get_string_value_with_interpolation(val, &injectables)).collect();
		for arg in &args
		{
			if arg.is_none() { Err(BoxErr::from("Each arg must be a string."))? }
		}
		let args: Vec<&String> = args.iter().map(|arg| arg.as_ref().unwrap()).collect();
		let env = &step["env"];
		let cases = match step["cases"].is_object()
		{
			true => Some(&step["cases"]),
			false => None,
		};
		let subtask = match step["subtask"].is_object()
		{
			true => Some(&step["subtask"]),
			false => None,
		};
		let mut query = selector.as_ref().and_then(|selector|
		{
			log!(false, "Finding element: {}", &selector);
			Some
			(
				driver.query(By::Css(&selector))
					.wait(timeout, std::time::Duration::from_secs(1))
			)
		});
		if query.is_some() && step_type.is_some_and(|step_type| step_type == "click")
		{
			query = Some(query.unwrap().and_clickable());
		}
		let elements = match &query
		{
			Some(query) => { query.all().await.ok() },
			None => None,
		};
		let element = elements.as_ref()
			.map_or(None, |elements| elements.get(0))
			.ok_or(format!("Could not find element: {}", selector.unwrap_or("<missing selector property>".to_string())));
		let complex_result = evaluate_complex_value
		(
			EvaluateComplexValueParams
			{
				driver,
				step,
				injectables,
				elements: &elements,
				code: code.as_ref(),
				args: &args,
			}
		).await.map_err(|e| format!("{:?}", e));
		match step_type.unwrap_or("print")
		{
			"print" =>
			{
				let printables = if step_type.is_some() {complex_result?} else {complex_result.unwrap_or(Vec::new())};
				for s in printables
				{
					log!(true, "{}", s);
				}
				Ok(())
			}
			"navigate" =>
			{
				let url = complex_value_get_single_result(&complex_result)?;
				log!(false, "NAVIGATING: {}", url);
				driver.goto(url).await?;
				Ok(())
			},
			"wait" =>
			{
				log!(false, "WAITING: {}", wait_for);
				match wait_for
				{
					"element" =>
					{
						element?;
						Ok(())
					},
					"timeout" =>
					{
						tokio::time::sleep(timeout).await;
						Ok(())
					},
					"finishedDownloading" =>
					{
						wait_for_download_finished(&injectables, timeout)
					},
					unknown => Err(BoxErr::from(format!("Unknown waitfor type: {}", unknown))),
				}
			},
			"click" =>
			{
				let element = element?;
				let tabbed = tabbed.unwrap_or(subtask.is_some());
				let scoped = scoped.unwrap_or(false);
				log!(false, "CLICKING: {}.{}", element.tag_name().await?, element.class_name().await?.unwrap_or(String::new()));
				element.click().await?;
				if tabbed
				{
					let handle = switch_to_last_tab(driver).await?;
					let subtask = subtask.ok_or("Step missing subtask property.")?;
					run_task(driver, subtask, injectables, scoped, false).await?;
					if handle.is_some()
					{
						close_tab(driver, handle.unwrap()).await?;
					}
				}
				Ok(())
			},
			"sendKeys" =>
			{
				let element = element?;
				let val = complex_result?.iter().fold(String::new(), |prev, next|
				{
					format!("{}{}", prev.as_str(), next.as_str())
				});
				element.send_keys(val).await?;
				Ok(())
			},
			"acceptAlert" =>
			{
				driver.accept_alert().await.map_err(|e| BoxErr::from(e))
			},
			"saveImage" =>
			{
				let element = element?;
				let file = file.ok_or("Step missing file property")?;
				let file = Path::new(&file);
				let dir = file.parent();
				if dir.is_some()
				{
					let dir = dir.unwrap();
					std::fs::create_dir_all(dir)?;
				}
				let tag_name = element.tag_name().await?;
				let class_name = element.class_name().await?.unwrap_or(String::new());
				let display_path = file.as_os_str().to_str().ok_or("Path could not be displayed.")?;
				log!(true, "Saving {}.{} to {}", tag_name, class_name, display_path);
				element.screenshot(file).await?;
				Ok(())
			},
			"saveFile" =>
			{
				let url = complex_value_get_single_result(&complex_result)?;
				let file = file.ok_or("Step missing file property")?;
				let file = Path::new(&file);
				let filename = file.file_name().ok_or("File property must be a valid file name or path.")?.to_str().ok_or("File property could not be stringified.")?;
				let mut dir = file.parent();
				let mut param_injects = HashMap::<String, String>::new();
				if dir.is_some()
				{
					let dir_str = dir.as_ref().unwrap().display().to_string();
					if !dir_str.is_empty()
					{
						log!(true, "Saving file as {}/{}: {}", dir_str.as_str(), filename, &url);
						param_injects.insert("cwd".to_string(), dir_str.to_owned());
						std::fs::create_dir_all(dir.unwrap())?;
					}
					else
					{
						dir.take();
					}
				}
				else
				{
					log!(true, "Saving file as {}: {}", &filename, &url);
				}
				param_injects.insert("url".to_string(), url.clone());
				param_injects.insert("filename".to_string(), filename.to_string());
				let code = interpolate_string
				(r#"
					let done = arguments[0];
					console.log('Starting file fetch...');
					fetch('{url\'}').
						then
						(
							r => r.blob().
								then
								(
									b =>
									{
										console.log('Fetch complete.');
										let a = document.createElement('a');
										a.href = URL.createObjectURL(new File([b], '{filename\'}'));
										a.download = '{filename\'}';
										a.click();
										done();
									}
								)
						)
					"#,
					&param_injects
				);
				driver.set_script_timeout(timeout).await?;
				driver.execute_async(code.as_str(), Vec::new()).await?;
				wait_for_download_finished(&injectables, timeout)?;
				if dir.is_some()
				{
					match std::fs::read_dir(injectables.get("downloadDir").unwrap())?
						.filter_map(|i| i.ok())
						.filter_map(|dir_entry| dir_entry.file_name().to_str().map(|s| (dir_entry, s.to_owned())))
						.filter(|(_, entry_filename)| entry_filename.starts_with(filename))
						.filter_map(|(dir_entry, entry_filename)|
						{
							param_injects.insert("entry_filename".to_string(), entry_filename);
							let from = dir_entry.path();
							let to = interpolate_string("{cwd}/{entry_filename}", &param_injects);
							match std::fs::rename(&from, &to).or_else(|_err|
							{
								std::fs::copy(&from, &to)?;
								std::fs::remove_file(&from)
							})
							{
								Ok(_) => None,
								Err(e) => Some(e)
							}
						})
						.next()
					{
						Some(e) => Err(e),
						None => Ok(())
					}?;
				};
				Ok(())
			},
			"upload" =>
			{
				let element = element?;
				let file = file.ok_or("Step missing file property")?;
				let file = Path::new(&file).canonicalize()?;
				let file = file.as_path().to_str().ok_or("Upload file was not valid:")?;
				log!(true, "UPLOADING: {}", file);
				match element.tag_name().await?.as_str()
				{
					"input" =>
					{
						match element.prop("type").await?.ok_or("Specified element must have a type property")?.as_str()
						{
							"file" =>
							{
								element.send_keys(file).await?;
								Ok(())
							},
							err_type => Err(BoxErr::from(format!("Specified element must be of type file. Got: {}", err_type)))
						}
					},
					name => Err(BoxErr::from(format!("Specified element must be a input tag. Got: {}", name))),
				}
			},
			"exec" =>
			{
				let exec = exec.ok_or("Step was missing exec property.")?;
				let cwd = cwd.unwrap_or(env::current_dir()?.to_str().ok_or("Program had no CWD.")?.to_string());
				let mut env_translated = HashMap::<String, String>::new();
				for (key, val) in env.entries()
				{
					let val = get_string_value_with_interpolation(val, &injectables).ok_or("Environment variable was invalid.")?;
					env_translated.insert(key.to_string(), val);
				}
				exec_simple(exec.as_str(), &args, &env_translated, cwd.as_str(), step_verbose)?;
				Ok(())
			}
			"setvar" =>
			{
				let key = key.ok_or("Step missing key property")?;
				let value = complex_value_get_single_result(&complex_result)?;
				log!(false, "Set variable: {} = {}", &key, value);
				injectables.insert(key, value.clone());
				Ok(())
			},
			"if" =>
			{
				let scoped = scoped.unwrap_or(false);
				let mut potential_new_scope = if scoped { injectables.clone() } else { std::collections::HashMap::<String, String>::new() };
				let injectables = if scoped { &mut potential_new_scope } else { &mut (*injectables) };
				let subtask = subtask.ok_or("Step missing subtask property")?;
				if complex_value_is_truthy(&complex_result) != not
				{
					if key.is_some()
					{
						let key = key.unwrap();
						let complex_result = complex_result?;
						let val = complex_result.get(0).unwrap();
						injectables.insert(key, val.clone());
					}
					let tabbed = tabbed.unwrap_or(false);
					run_task(driver, subtask, injectables, scoped, tabbed).await?;
				}
				Ok(())
			},
			"switch" =>
			{
				let match_value = match_value.ok_or("Step missing match property")?;
				let cases = cases.ok_or("Step missing cases property.")?;
				if cases.has_key(match_value.as_str())
				{
					let subtask = &cases[match_value];
					let tabbed = tabbed.unwrap_or(false);
					let scoped = scoped.unwrap_or(false);
					run_task(driver, subtask, injectables, scoped, tabbed).await?;
					Ok(())
				}
				else { Err(BoxErr::from(format!("Could not find case for: {}", match_value)))? }
			},
			"foreach" =>
			{
				let tabbed = tabbed.unwrap_or(false);
				let subtask = subtask.ok_or("Step missing subtask property")?;
				let key = key.ok_or("Step missing key property")?;
				for val in complex_result?
				{
					match scoped.unwrap_or(true)
					{
						true =>
						{
							let mut injectables = injectables.clone();
							injectables.insert(key.clone(), val);
							run_task(driver, subtask, &mut injectables, false, tabbed).await
						}
						false =>
						{
							injectables.insert(key.clone(), val);
							run_task(driver, subtask, injectables, false, tabbed).await
						}
					}?;
				}
				Ok(())
			},
			"while" =>
			{
				let scoped = scoped.unwrap_or(false);
				let mut potential_new_scope = if scoped { injectables.clone() } else { std::collections::HashMap::<String, String>::new() };
				let injectables = if scoped { &mut potential_new_scope } else { &mut (*injectables) };
				let subtask = subtask.ok_or("Step missing subtask property")?;
				if complex_value_is_truthy(&complex_result)
				{
					if key.is_some()
					{
						let key = key.unwrap();
						let complex_result = complex_result?;
						let val = complex_result.get(0).unwrap();
						injectables.insert(key, val.clone());
					}
					let tabbed = tabbed.unwrap_or(false);
					run_task(driver, subtask, injectables, scoped, tabbed).await?;
					i -= 1;
				}
				Ok(())
			},
			"exit" =>
			{
				let exit_code = exit_code.ok_or("exit_code property must be a positive number.")?;
				std::process::exit(exit_code.into());
			},
			unknown => Err(BoxErr::from(format!("Unknown task type: {}", unknown))),
		}?;
		i += 1;
	}
	Ok(())
}

fn launch_server() -> Result<Child, BoxErr>
{
	let mut cmd = Command::new("geckodriver");
	let verbose = unsafe { VERBOSE };
	let cmd = cmd
		.stdout(Stdio::null())
		.stderr(match verbose
			{
				true => Stdio::inherit(),
				false => Stdio::null()
			}
		);
	cmd.spawn().or_else(|err|
	{
		match err.kind()
		{
			std::io::ErrorKind::NotFound =>
			{
				log!(true, "Could not find geckodriver. Attempting to install with cargo.");
				Command::new("cargo")
					.args(["install", "geckodriver"])
					.stdout(Stdio::inherit())
					.stderr(Stdio::inherit())
					.output()
					.expect("Could not launch cargo install")
					.status
					.exit_ok()
					.expect("Unknown error during install. Try running 'cargo install geckodriver' manually.");
				cmd.spawn().or_else(|err|
				{
					match err.kind()
					{
						ErrorKind::NotFound => Err(std::io::Error::new(ErrorKind::NotFound, "Installed geckodriver, but could not find it. PATH variable likely needs updating.")),
						_ => Err(err),
					}
				})
			},
			_ => Err(err),
		}
	}).map_err(|err| -> BoxErr { Box::new(err) })
}

fn exec_simple(exec: &str, args: &Vec<&String>, env: &HashMap<String, String>, cwd: &str, verbose: bool) -> Result<(), BoxErr>
{
	log!(true, "EXECUTE: {} {:?}", exec, args);
	std::fs::create_dir_all(cwd)?;
	let verbose = verbose || unsafe { VERBOSE };
	let status = Command::new(exec)
		.args(args)
		.envs(env)
		.current_dir(cwd)
		.stdout(match verbose
			{
				true => Stdio::inherit(),
				false => Stdio::null()
			})
		.stderr(match verbose
			{
				true => Stdio::inherit(),
				false => Stdio::null()
			})
		.status()?;
	status.exit_ok()?;
	Ok(())
}

struct EvaluateComplexValueParams<'a>
{
	driver: &'a WebDriver,
	step: &'a JsonValue,
	injectables: &'a std::collections::HashMap<String, String>,
	elements: &'a Option<Vec<WebElement>>,
	code: Option<&'a std::string::String>,
	args: &'a Vec<&'a String>,
}

async fn evaluate_complex_value(params: EvaluateComplexValueParams<'_>) -> Result<Vec<String>, BoxErr>
{
	let value_type = get_string_value_with_interpolation(&params.step["valueType"], &params.injectables);
	let fmt = get_string_value_with_interpolation(&params.step["fmt"], &params.injectables);
	let value = get_string_value_with_interpolation(&params.step["value"], &params.injectables);
	let property_name = get_string_value_with_interpolation(&params.step["propertyName"], &params.injectables);
	let value_type = value_type.unwrap_or("constant".to_string());
	let selector_elements = params.elements.as_ref().ok_or("Could not find elements or step missing selector property");
	let code_result = match params.code
	{
		None => None,
		Some(code) =>
		{
			
			let mut encoded_args = Vec::<serde_json::Value>::new();
			for arg in params.args
			{
				let arg = arg.to_owned();
				let arg = serde_json::to_value(arg.as_str())?;
				encoded_args.insert(encoded_args.len(), arg);
			}
			log!(false, "JAVASCRIPT: {} ... {:?}", code, params.args);
			Some(params.driver.execute(code.as_str(), encoded_args).await?)
		}
	};
	let code_result_strings = match code_result.as_ref()
	{
		None => Vec::<String>::new(),
		Some(result) =>
		{
			type ScriptRet=thirtyfour::session::scriptret::ScriptRet;
			vec!
			(
				ScriptRet::convert::<bool>(&result).map(|b|
				{
					vec!(match b
					{
						true => "true",
						false => "false"
					}.to_string())
				}),
				ScriptRet::convert::<String>(&result).map(|s| vec!(s)),
				ScriptRet::convert::<Vec<String>>(&result)
			)
				.iter_mut()
				.filter(|r| r.is_ok())
				.flat_map(|r| r.as_mut().unwrap().to_owned())
				.collect()
		}
	};
	let code_result_elements = match code_result
	{
		None => None,
		Some(result) =>
		{
			match result.json().is_array()
			{
				true => result.elements(),
				false => result.element().map(|e| vec!(e)),
			}.ok()
		}
	};
	let elements = match code_result_elements.as_ref()
	{
		None => Ok(selector_elements),
		Some(code_result_elements) =>
		{
			match selector_elements
			{
				Ok(_) => Err("Step provided both selector and javascript."),
				Err(_) => Ok(Ok(code_result_elements))
			}
		}
	}?;
	let mut ret_val = Vec::<String>::new();
	match value_type.as_str()
	{
		"innerHTML" =>
		{
			for element in elements? { ret_val.push(element.inner_html().await?.trim().to_string()); }
		}
		"elementText" =>
		{
			for element in elements? { ret_val.push(element.text().await?.trim().to_string()); }
		}
		"elementProperty" =>
		{
			let property_name = property_name.ok_or("Step was missing propertyName property.")?;
			for element in elements? { ret_val.push(element.prop(&property_name).await?.ok_or("Could not determine element property.")?.trim().to_string()); }
		}
		"elementID" =>
		{
			for element in elements?
			{
				params.driver.execute(
					r#"
						window.__GENERATE_ID = window.__GENERATE_ID ?? 0;
						arguments[0].id = arguments[0].id || `auto-generated-id-${window.__GENERATE_ID++}`;
						return arguments[0].id;
					"#,
					vec![element.to_json()?]
				).await?;
				ret_val.push(element.id().await?.ok_or("Element refused ID generation.")?);
			}
		}
		"dateParser" =>
		{
			let value = value.ok_or("Step missing value property.")?;
			let fmt = fmt.ok_or("Step missing fmt property.")?;
			let date = parse_date_string_heuristically(&value)?;
			ret_val.push(date.format(fmt.as_str()).to_string());
		}
		"constant" =>
		{
			if value.is_some() {ret_val.push(value.unwrap())}
			else if !code_result_strings.is_empty()
			{
				for s in code_result_strings {ret_val.push(s);}
			}
			else if elements.is_ok()
			{
				elements?.iter().for_each(|_| ret_val.push("<element>".to_string()));
			}
			else {Err("Step missing value property.")?}
		}
		unknown => Err(format!("Unknown valueType: {}", unknown))?,
	};
	Ok(ret_val)
}

fn complex_value_is_truthy(complex_value: &Result<Vec<String>, String>) -> bool
{
	match complex_value
	{
		Err(_) => false,
		Ok(complex_value) =>
			complex_value.iter().any
			(
				|val|
					!val.is_empty() &&
					val != "false"
			)
	}
}

fn complex_value_get_single_result(complex_value: &Result<Vec<String>, String>) -> Result<&String, BoxErr>
{
	let complex_value = complex_value.as_ref().map_err(|e| e.to_owned())?;
	if complex_value.len() == 1
	{
		Ok(complex_value.first().unwrap())
	}
	else
	{
		Err(format!("Expected 1 value, got {}.", complex_value.len()))?
	}
}

fn parse_date_string_heuristically(str: &str) -> Result<chrono::NaiveDate, BoxErr>
{
	let str = str.to_uppercase();
	let colloquial_regex = Regex::new(r"^(TODAY|YESTERDAY|JUST NOW)( AT|\\.)?")?;
	let colloquial_date = colloquial_regex.captures(&str).and_then(|captures|
	{
		let word = captures.get(1).expect("Capture group failed.");
		let word = word.as_str();
		match word
		{
			"JUST NOW" | "TODAY" => Ok(chrono::Local::now().date_naive()),
			"YESTERDAY" => Ok(chrono::Local::now().checked_sub_signed(chrono::Duration::days(1)).unwrap().date_naive()),
			_ => Err(()),
		}.ok()
	});
	let relative_regex = Regex::new(r"^([0-9]+|AN) (S?[A-RT-Z]+)S? AGO")?;
	let relative_date = relative_regex.captures(&str).and_then(|captures|
	{
		let amt = captures.get(1).expect("Capture group failed.");
		let amt: i64 = match amt.as_str()
		{
			"AN" => 1,
			s => s.parse().expect("Capture group did not give int."),
		};
		let increment = captures.get(2).expect("Capture group failed.");
		let increment = increment.as_str();
		match increment
		{
			"DAY" => Ok(chrono::Duration::days(amt.try_into().unwrap())),
			"HOUR" => Ok(chrono::Duration::hours(amt.try_into().unwrap())),
			"MINUTE" => Ok(chrono::Duration::minutes(amt.try_into().unwrap())),
			_ => Err(())
		}.map(|offset|
		{
			chrono::Local::now().checked_sub_signed(offset).unwrap().date_naive()
		}).ok()
	});
	let absolute_date = Regex::new(r"^([A-Z]+) ([0-9]+)(, ([0-9]{4}))?( AT .*)?$")?;
	let absolute_date = absolute_date.captures(&str).and_then(|captures|
	{
		let month = captures.get(1).expect("Capture group failed.");
		let month = get_month_num_from_str(&month.as_str().to_string()).expect("Capture group did not give int.");
		let day = captures.get(2).expect("Capture group failed.");
		let day: u32 = day.as_str().parse().expect("Capture group did not give int.");
		let year: i32 = match captures.get(4)
		{
			Some(cap) => cap.as_str().to_string(),
			None => chrono::Utc::now().year().to_string()
		}.as_str().parse().expect("Capture group did not give int.");
		chrono::NaiveDate::from_ymd_opt(year, month, day)
	});
	Ok(colloquial_date.or(relative_date).or(absolute_date).ok_or(format!("Could not parse date: {str}."))?)
}

fn get_month_num_from_str(str: &String) -> Result<u32, ()>
{
	match str.to_lowercase().get(0..3)
	{
		Some(str) =>
		{
			match str
			{
				"jan" => Ok(1),
				"feb" => Ok(2),
				"mar" => Ok(3),
				"apr" => Ok(4),
				"may" => Ok(5),
				"jun" => Ok(6),
				"jul" => Ok(7),
				"aug" => Ok(8),
				"sep" => Ok(9),
				"oct" => Ok(10),
				"nov" => Ok(11),
				"dec" => Ok(12),
				_ => Err(()),
			}
		}
		None => Err(())
	}
}

fn wait_for_download_finished(injectables: &std::collections::HashMap<String, String>, timeout: Duration) -> Result<(), BoxErr>
{
	let download_dir = injectables.get("downloadDir").unwrap();
	let time_start = Instant::now();
	loop
	{
		let mut found_any = false;
		let mut found_part = false;
		let mut paths = std::fs::read_dir(download_dir)?;
		match paths.find_map(|path|
		{
			match path
			{
				Ok(path) =>
				{
					found_any = true;
					let path = path.path();
					let extension = path.extension().map(|s| s.to_str().unwrap_or("")).unwrap_or("");
					if extension == "part" { found_part = true; }
					None
				},
				Err(err) => Some(BoxErr::from(err))
			}
		})
		{
			Some(err) => Err(err),
			None => Ok(()),
		}?;
		if found_any && !found_part { break; }
		else if time_start.elapsed().gt(&timeout) { Err("Download did not finish within timeout.")?; }
	};
	Ok(())
}